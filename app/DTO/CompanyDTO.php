<?php

namespace App\DTO;

use App\Http\Requests\CompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;

class CompanyDTO
{
    public string $name;
    public string $email;
    public ?string $logoUrl;
    public ?string $website;

    public static function fromRequest(CompanyRequest|UpdateCompanyRequest $request): self
    {
        $self = new self;
        $self->name = $request->input('name');
        $self->email = $request->input('email');
        $self->website = $request->input('website');
        $self->logoUrl = null;

        if($request->hasFile('logo_url')) {
            $self->logoUrl = $request->file('logo_url')?->store('companies', 'public');
        }

        return $self;
    }
}
