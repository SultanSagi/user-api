<?php

namespace App\DTO;

use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;

class UserDTO
{
    public string $firstName;
    public string $lastName;
    public string $email;
    public string $phone;
    public string $password;
    public ?int $companyId;

    public static function fromRequest(UserRequest|UpdateUserRequest $request): self
    {
        $self = new self;
        $self->firstName = $request->input('first_name');
        $self->lastName = $request->input('last_name');
        $self->email = $request->input('email');
        $self->phone = $request->input('phone');
        $self->companyId = (int) $request->input('company_id');
        $self->password = 'password';

        return $self;
    }
}
