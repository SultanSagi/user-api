<?php

namespace App\Contracts\Repositories;

use App\DTO\CompanyDTO;
use App\Models\Company;

interface CompanyRepository
{
    public function create(CompanyDTO $companyDTO): void;
    public function update(Company $company, CompanyDTO $companyDTO): void;
}
