<?php

namespace App\Contracts\Repositories;

use Illuminate\Database\Eloquent\Collection;

interface UserQueryRepository
{
    public function get(): Collection;
}
