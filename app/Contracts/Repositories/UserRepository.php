<?php

namespace App\Contracts\Repositories;

use App\DTO\UserDTO;
use App\Models\User;

interface UserRepository
{
    public function create(UserDTO $userDTO): void;
    public function update(User $user, UserDTO $userDTO): void;
}
