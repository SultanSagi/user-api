<?php

namespace App\Contracts\Repositories;

use Illuminate\Database\Eloquent\Collection;

interface CompanyQueryRepository
{
    public function get(): Collection;
}
