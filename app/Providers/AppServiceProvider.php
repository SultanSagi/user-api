<?php

namespace App\Providers;

use App\Contracts\Repositories\CompanyQueryRepository;
use App\Contracts\Repositories\CompanyRepository;
use App\Contracts\Repositories\UserQueryRepository;
use App\Contracts\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(CompanyQueryRepository::class, \App\Repositories\CompanyQueryRepository::class);
        $this->app->bind(CompanyRepository::class, \App\Repositories\CompanyRepository::class);
        $this->app->bind(UserQueryRepository::class, \App\Repositories\UserQueryRepository::class);
        $this->app->bind(UserRepository::class, \App\Repositories\UserRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
