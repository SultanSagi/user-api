<?php

namespace App\Repositories;

use App\Contracts\Repositories\UserQueryRepository as UserQueryRepositoryContract;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserQueryRepository implements UserQueryRepositoryContract
{

    public function get(): Collection
    {
        return User::get();
    }
}
