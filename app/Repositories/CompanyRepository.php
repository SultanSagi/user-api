<?php

namespace App\Repositories;

use App\Contracts\Repositories\CompanyRepository as CompanyRepositoryContract;
use App\DTO\CompanyDTO;
use App\Models\Company;

class CompanyRepository implements CompanyRepositoryContract
{
    public function create(CompanyDTO $companyDTO): void
    {
        Company::create([
            'name' => $companyDTO->name,
            'email' => $companyDTO->email,
            'logo_url' => $companyDTO->logoUrl,
            'website' => $companyDTO->website,
        ]);
    }

    public function update(Company $company, CompanyDTO $companyDTO): void
    {
        if ($companyDTO->logoUrl) {
            $company->logo_url = $companyDTO->logoUrl;
        }

        $company->name = $companyDTO->name;
        $company->email = $companyDTO->email;
        $company->website = $companyDTO->website;
        $company->save();
    }
}
