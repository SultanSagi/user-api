<?php

namespace App\Repositories;

use App\Contracts\Repositories\CompanyQueryRepository as CompanyQueryRepositoryContract;
use App\Models\Company;
use Illuminate\Database\Eloquent\Collection;

class CompanyQueryRepository implements CompanyQueryRepositoryContract
{

    public function get(): Collection
    {
        return Company::get();
    }
}
