<?php

namespace App\Repositories;

use App\Contracts\Repositories\UserRepository as UserRepositoryContract;
use App\DTO\UserDTO;
use App\Models\User;

class UserRepository implements UserRepositoryContract
{
    public function create(UserDTO $userDTO): void
    {
        User::create([
            'first_name' => $userDTO->firstName,
            'last_name' => $userDTO->lastName,
            'company_id' => $userDTO->companyId,
            'email' => $userDTO->email,
            'phone' => $userDTO->phone,
            'password' => $userDTO->password,
        ]);
    }

    public function update(User $user, UserDTO $userDTO): void
    {
        $user->first_name = $userDTO->firstName;
        $user->last_name = $userDTO->lastName;
        $user->email = $userDTO->email;
        $user->phone = $userDTO->phone;
        $user->company_id = $userDTO->companyId;
        $user->save();
    }
}
