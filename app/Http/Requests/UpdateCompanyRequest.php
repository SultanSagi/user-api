<?php

namespace App\Http\Requests;

use App\DTO\CompanyDTO;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'sometimes|required|string',
            'email' => 'sometimes|required|string|unique:companies,email,'.$this->company->id,
            'logo_url' => 'nullable|image|mimes:jpg,jpeg,png',
            'website' => 'nullable',
        ];
    }

    public function getDTO(): CompanyDTO
    {
        return CompanyDTO::fromRequest($this);
    }
}
