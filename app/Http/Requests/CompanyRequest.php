<?php

namespace App\Http\Requests;

use App\DTO\CompanyDTO;
use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|unique:companies,email',
            'logo_url' => 'nullable|image|mimes:jpg,jpeg,png',
            'website' => 'nullable',
        ];
    }

    public function getDTO(): CompanyDTO
    {
        return CompanyDTO::fromRequest($this);
    }
}
