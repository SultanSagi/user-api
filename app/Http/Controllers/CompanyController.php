<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\CompanyQueryRepository;
use App\Contracts\Repositories\CompanyRepository;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Http\Resources\CompanyCollection;
use App\Http\Resources\CompanyResource;
use App\Models\Company;

class CompanyController extends Controller
{
    public function __construct(
        private readonly CompanyRepository $companyRepository,
        private readonly CompanyQueryRepository $companyQueryRepository,
    ) {
    }

    public function store(CompanyRequest $request)
    {
//        if($request->hasFile('logo_url')) {
//            $request->logo_url = $request->file('logo_url')->store('companies', 'public');
//        }
//
//        Company::create([
//            'name' => $request->name,
//            'email' => $request->email,
//            'logo_url' => $request->logo_url,
//            'website' => $request->website,
//        ]);
        $this->companyRepository->create($request->getDTO());

        return response()->json([
            'message' => 'Company successfully created',
        ], 201);
    }

    public function index()
    {
        $companies = $this->companyQueryRepository->get();

        return new CompanyCollection($companies);
    }

    public function show(Company $company)
    {
        return new CompanyResource($company);
    }

    public function update(UpdateCompanyRequest $request, Company $company)
    {
//        if($request->hasFile('logo_url')) {
//            $company->logo_url = $request->file('logo_url')->store('companies', 'public');
//        }
//
//        $company->name = $request->name;
//        $company->email = $request->email;
//        $company->website = $request->website;
//        $company->save();
        $this->companyRepository->update($company, $request->getDTO());

        return new CompanyResource($company);
    }

    public function destroy(Company $company)
    {
        $company->delete();

        return response()->json([], 204);
    }
}
