<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\UserQueryRepository;
use App\Contracts\Repositories\UserRepository;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;

class UserController extends Controller
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserQueryRepository $userQueryRepository,
    ) {
    }

    public function store(UserRequest $request)
    {
//        User::create([
//            'first_name' => $request->first_name,
//            'last_name' => $request->last_name,
//            'company_id' => $request->company_id,
//            'email' => $request->email,
//            'phone' => $request->phone,
//            'password' => bcrypt('password'),
//        ]);
        $this->userRepository->create($request->getDTO());

        return response()->json([
            'message' => 'User successfully created',
        ], 201);
    }

    public function index()
    {
        $users = $this->userQueryRepository->get();

        return new UserCollection($users);
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
//        $user->update($request->all());
        $this->userRepository->update($user, $request->getDTO());

        return new UserResource($user);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([], 204);
    }
}
