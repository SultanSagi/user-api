<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_be_created(): void
    {
        $company = Company::factory()->create();

        $params = [
            'first_name' => 'Max',
            'last_name' => 'Steve',
            'company_id' => $company->id,
            'email' => 'max@ex.org',
            'phone' => '10001002020',
        ];

        $response = $this->post('/api/users', $params);

        $response->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'company_id' => $params['company_id'],
            'email' => $params['email'],
            'phone' => $params['phone'],
        ]);
    }

    /** @test */
    public function we_can_view_all_users(): void
    {
        User::factory()->create();

        $response = $this->get('api/users');

        $response->assertJsonStructure([
            'data' => [
                [
                    'first_name',
                    'last_name',
                    'email',
                    'company' => [
                        'name',
                        'email',
                    ],
                ],
            ],
        ]);
    }

    /** @test */
    public function we_can_view_an_user_detail(): void
    {
        $user = User::factory()->create();

        $response = $this->get('api/users/' . $user->id);

        $response->assertSee($user->first_name);
        $response->assertJsonStructure([
            'data' => [
                'first_name',
                'last_name',
                'email',
                'company' => [
                    'name',
                    'email',
                ],
            ],
        ]);
    }

    /** @test */
    public function user_can_be_updated(): void
    {
        $user = User::factory()->create();

        $params = [
            'first_name' => 'Update',
            'last_name' => 'Last',
            'email' => 'newemail@ex.org',
            'phone' => '000000122',
        ];

        $this->put('/api/users/' . $user->id, $params);

        tap($user->fresh(), function ($user) use ($params) {
            $this->assertEquals($params['first_name'], $user->first_name);
            $this->assertEquals($params['last_name'], $user->last_name);
            $this->assertEquals($params['email'], $user->email);
            $this->assertEquals($params['phone'], $user->phone);
        });
    }

    /** @test */
    public function user_can_be_deleted(): void
    {
        $user = User::factory()->create();

        $this->delete('/api/users/' . $user->id);

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
        ]);
    }
}
