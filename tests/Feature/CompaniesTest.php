<?php

namespace Tests\Feature;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CompaniesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function company_can_be_created(): void
    {
        Storage::fake('public');

        $file = UploadedFile::fake()->image('company.jpg');

        $params = [
            'name' => 'Max',
            'email' => 'max@ex.org',
            'logo_url' => $file,
            'website' => 'http://google.org',
        ];

        $response = $this->post('/api/companies', $params);

        $response->assertStatus(201);

        $this->assertDatabaseHas('companies', [
            'name' => $params['name'],
            'email' => $params['email'],
            'website' => $params['website'],
        ]);

        Storage::disk('public')->assertExists('companies/'.$file->hashName());
    }

    /** @test */
    public function we_can_view_all_companies(): void
    {
        Company::factory()->create();

        $response = $this->get('api/companies');

        $response->assertJsonStructure([
            'data' => [
                [
                    'name',
                    'email',
                    'logo_url',
                    'website',
                ],
            ],
        ]);
    }

    /** @test */
    public function we_can_view_a_company_detail(): void
    {
        $company = Company::factory()->create();

        $response = $this->get('api/companies/' . $company->id);

        $response->assertSee($company->name);
        $response->assertJsonStructure([
            'data' => [
                'name',
                'email',
                'logo_url',
                'website',
            ],
        ]);
    }

    /** @test */
    public function company_can_be_updated(): void
    {
        $company = Company::factory()->create();

        $params = [
            'name' => 'Update',
            'email' => 'updated@mail.co',
        ];

        $response = $this->post('/api/companies/' . $company->id, $params);

        tap($company->fresh(), function ($company) use ($params) {
            $this->assertEquals($params['name'], $company->name);
        });
    }

    /** @test */
    public function company_can_be_deleted(): void
    {
        $company = Company::factory()->create();

        $this->delete('/api/companies/' . $company->id);

        $this->assertDatabaseMissing('companies', [
            'id' => $company->id,
        ]);
    }
}
