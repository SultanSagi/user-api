<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('users', [\App\Http\Controllers\UserController::class, 'store']);
Route::get('users', [\App\Http\Controllers\UserController::class, 'index']);
Route::get('users/{user}', [\App\Http\Controllers\UserController::class, 'show']);
Route::put('users/{user}', [\App\Http\Controllers\UserController::class, 'update']);
Route::delete('users/{user}', [\App\Http\Controllers\UserController::class, 'destroy']);

Route::post('companies', [\App\Http\Controllers\CompanyController::class, 'store']);
Route::get('companies', [\App\Http\Controllers\CompanyController::class, 'index']);
Route::get('companies/{company}', [\App\Http\Controllers\CompanyController::class, 'show']);
Route::post('companies/{company}', [\App\Http\Controllers\CompanyController::class, 'update']);
Route::delete('companies/{company}', [\App\Http\Controllers\CompanyController::class, 'destroy']);
